import { View } from 'react-native';

export default function Dot ({active}) {
    return (
        <View style={{borderColor: "#20588b", borderRadius: 20, borderWidth: 1.4, backgroundColor:active ? "#20588b" : undefined, height: 10, width: 10, marginRight: 4}}/>
    )
}