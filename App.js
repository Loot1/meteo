import { useState, useEffect, useCallback } from 'react';
import { StatusBar } from 'expo-status-bar';
import { View, Text, ScrollView, SafeAreaView, Image, RefreshControl } from 'react-native';
import { Lexend_200ExtraLight, Lexend_400Regular, Lexend_600SemiBold } from '@expo-google-fonts/lexend';
import { LinearGradient } from 'expo-linear-gradient';
import * as SplashScreen from 'expo-splash-screen';
import * as Font from 'expo-font';
import fetch from 'node-fetch'
import Dot from './src/components/Dot'
import { useSwipe } from './src/hooks/useSwipe';

const apiKey = "38c269fe1ad979d1031a088c3091d8d2"
const daysName = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam",]

export default function App() {
    const [appIsReady, setAppIsReady] = useState(false)
    const [data, setData] = useState(null)
    const [refreshing, setRefreshing] = useState(false)
    const date = new Date()
    const dateDate = date.getDate()
    const dateHour = date.getHours()

    const [cities, setCities] = useState(["Amiens", "Paris", "Nice"])
    const [cityIndex, setCityIndex] = useState(0)

    const { onTouchStart, onTouchEnd } = useSwipe(onSwipeLeft, onSwipeRight, 3)

    function onSwipeLeft(){
        if(cityIndex < cities.length - 1) {
            getWeather(cities[cityIndex + 1])
            setCityIndex(cityIndex + 1)
        }
    }

    function onSwipeRight(){
        if(cityIndex >= (cities.length - 1) - cityIndex) {
            getWeather(cities[cityIndex - 1])
            setCityIndex(cityIndex - 1)
        }
    }

    const onRefresh = useCallback(() => {
        setRefreshing(true)
        getWeather()
    }, []);

    const getCoordinates = async (city) => {
        let coords = await fetch(`http://api.openweathermap.org/geo/1.0/direct?q=${city}&appid=${apiKey}`)
        .catch(e => console.log(e))
        coords = await coords.json()
        coords = coords[0]
        return {lat:coords.lat,lon:coords.lon}
    }

    const getWeather = async (city = cities[cityIndex]) => {
        const { lat, lon } = await getCoordinates(city)
        let weather = await fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&units=metric&exclude=minutely,alerts&lang=fr&appid=${apiKey}`)
        .catch(e => console.log(e))
        .finally(() => setRefreshing(false))
        weather = await weather.json()
        setData(weather)
    }

    useEffect(() => {
        async function prepare() {
            try {
                await SplashScreen.preventAutoHideAsync()
                await Font.loadAsync({ Lexend_200ExtraLight, Lexend_400Regular, Lexend_600SemiBold })
            } catch (e) {
                console.warn(e)
            } finally {
                setAppIsReady(true)
            }
        }
        prepare()
        getWeather()
    }, [])

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }

    const onLayoutRootView = useCallback(async () => {
        if (appIsReady) await SplashScreen.hideAsync()
    }, [appIsReady])

    if (!appIsReady) return null

    return (
        <LinearGradient
            colors={['#a6dff1', '#2c79c1']}
            start={{ x: 0, y: 0.3 }}
            end={{ x: 1, y: 1 }}
            style={{ flex: 1 }}
            onLayout={onLayoutRootView}
        >
            <SafeAreaView style={{ flex: 1 }}>
                <StatusBar style="light"/>
                <ScrollView
                    style={{ flex: 1, paddingHorizontal: 20 }}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                            colors={["white"]}
                            tintColor="white"
                        />
                    }
                    onTouchStart={onTouchStart}
                    onTouchEnd={onTouchEnd}
                >
                    <View style={{flexDirection:"row", paddingHorizontal: 40, justifyContent:"space-between", marginTop: 20}}>
                        <View style={{flex:1, flexDirection:"column", alignItems:"center"}}>
                            <Text style={{ fontFamily: "Lexend_600SemiBold", color: "#20588b", fontSize:16}}>{cities[cityIndex]}</Text>
                            <View style={{flexDirection:"row", marginTop: 5}}>
                                {cities.map((ci) => (
                                    <Dot active={cities[cityIndex] === ci ? true : false} key={ci}/>
                                ))}
                            </View>
                        </View>
                    </View>
                    <Image source={require("./assets/images/icon.png")} style={{ alignSelf: "center", top: -15 }} />
                    <View style={{ flex: 1, padding: 20, backgroundColor: "#327dc2", borderRadius: 12, alignItems: "center" }}>
                        <Text style={{ fontFamily: "Lexend_600SemiBold", color: "white", fontSize: 70, marginBottom: 5 }}>{data && parseInt(data.current.temp)}°</Text>
                        <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", fontSize: 17 }}>{data && capitalizeFirstLetter(data.current.weather[0].description)}</Text>
                        <View style={{ flexDirection: "row", marginTop: 15, marginBottom: 5 }}>
                            <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", borderRightWidth: 1, borderRightColor: "white", paddingVertical: 5 }}>
                                <Image source={require("./assets/images/wind.png")} style={{ marginRight: 10 }} />
                                <View>
                                    <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", fontSize: 14 }}>{data && parseInt(data.current.wind_speed)} km/h</Text>
                                    <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", fontSize: 14 }}>Vent</Text>
                                </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", paddingVertical: 5 }}>
                                <Image source={require("./assets/images/rain.png")} style={{ marginRight: 10 }} />
                                <View>
                                    <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", fontSize: 14 }}>{data && data.current.humidity} %</Text>
                                    <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", fontSize: 14 }}>Pluie</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <ScrollView horizontal={true} style={{ flex: 1, marginTop: 30, padding: 15, backgroundColor: "#327dc2", borderRadius: 12 }}>
                        {data && data.hourly.map((hour, index) => {
                            let hourDate = date
                            hourDate.setHours(dateHour + index)
                            if(index < 25) {
                                return (
                                    <View style={{ marginRight: 20, alignItems: "center" }} key={index}>
                                        <Text style={{ fontFamily: "Lexend_600SemiBold", color: "white", marginBottom: 10 }}>{index === 0 ? "Maintenant" : hourDate.getHours() + " h"}</Text>
                                        <Image source={require("./assets/images/3.png")} style={{ marginBottom: 5 }} />
                                        <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", marginBottom: 5, fontSize: 13 }}>{data && parseInt(hour.temp)}°</Text>
                                        <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", fontSize: 13 }}>{data && hour.humidity}% pluie</Text>
                                    </View>
                                )
                            }
                        })}
                    </ScrollView>
                    <View style={{ flex: 1, marginTop: 30, marginBottom:30, padding: 15, backgroundColor: "#327dc2", borderRadius: 12 }}>
                        <Text style={{ fontFamily: "Lexend_600SemiBold", color: "white", marginBottom: 25 }}>Prévisions sur 8 jours</Text>
                        {data && data.daily.map((day, index) => {
                            let dayDate = date
                            dayDate.setDate(dateDate + index)
                            return (
                                <View style={{ borderBottomColor: index === 7 ? null : "white", borderBottomWidth: index === 7 ? null : 1, flexDirection: "row", paddingHorizontal: 10, marginBottom: index === 7 ? null : 10 }} key={index}>
                                    <Text style={{ flex: 1, fontFamily: "Lexend_600SemiBold", color: "white", marginBottom: 10 }}>{index === 0 ? "Auj" : daysName[dayDate.getDay()]}.</Text>
                                    <View style={{ flex: 1, flexDirection: "row", justifyContent: "center" }}>
                                        <Image source={require("./assets/images/4.png")} style={{ marginBottom: 5, marginRight: 10, bottom: 2 }} />
                                        <Text style={{ fontFamily: "Lexend_200ExtraLight", color: "white", fontSize: 14 }}>{data && parseInt(day.humidity)}% pluie</Text>
                                    </View>
                                    <Text style={{ flex: 1, textAlign: "right", fontFamily: "Lexend_200ExtraLight", color: "white", marginBottom: 5, fontSize: 13 }}>{data && parseInt(day.temp.min)}°/{data && parseInt(day.temp.max)}°</Text>
                                </View>
                            )
                        })}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
}
